
function TestAnimation(resources)
{
	TestAnimation.resources = resources;
}
TestAnimation.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(1000, 400, Phaser.CANVAS, 'TestAnimation', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this});
	},

	preload: function()
	{
		this.game.scale.maxWidth = 1000;
    	this.game.scale.maxHeight = 400;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
    	this.game.load.image('leftImage', TestAnimation.resources.leftImage);
    	this.game.load.image('centerImage', TestAnimation.resources.centerImage);
    	this.game.load.image('rightImage', TestAnimation.resources.rightImage);
    	
    	this.game.stage.backgroundColor = '#ffffff'
    	
	},

	create: function(evt)
	{
		this.game.stage.backgroundColor = '#ffffff'
		this.parent.leftImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'leftImage');
		this.parent.leftImage.alpha = 1; 
		this.parent.leftImage.smoothed = false;
		this.parent.centerImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'centerImage');
		this.parent.centerImage.alpha = 1;
		this.parent.centerImage.smoothed = false;
		
		this.parent.rightImage = this.game.add.sprite(this.game.world.centerX,this.game.world.centerY, 'rightImage');
		this.parent.rightImage.alpha = 1; 
		this.parent.rightImage.smoothed = false; 
		
		var style = TestAnimation.resources.textStyle_1;
		var style2 = TestAnimation.resources.textStyle_2;
		this.parent.leftText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation.resources.leftText, style);
		this.parent.centerText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation.resources.centerText, style);
		this.parent.rightText = this.game.add.text(this.game.world.centerX,this.game.world.centerY, TestAnimation.resources.rightText, style);
		this.parent.buildAnimation();
		this.parent.leftText.smoothed = false;
		this.parent.centerText.smoothed = false;
		this.parent.rightText.smoothed = false;

	},

	buildAnimation: function()
	{
		
		this.rightImage.anchor.set(0.5);
		this.centerImage.anchor.set(0.5);
		this.leftImage.anchor.set(0.5);
		this.leftText.anchor.set(0.5);
		this.centerText.anchor.set(0.5);
		this.rightText.anchor.set(0.5);
		//
		
		
		this.rightText.x = this.game.world.centerX+350;
		this.rightText.y = this.game.world.centerY;
		this.rightText.originX = this.rightText.x;
		this.rightText.alpha = 0;

		this.centerText.alpha = 0;
		this.centerText.x = this.game.world.centerX-0;
		this.centerText.y = this.game.world.centerY;
		this.centerText.originX = this.centerText.x;
		

		this.leftText.alpha = 0;
		this.leftText.x = this.game.world.centerX-200;
		this.leftText.y = this.game.world.centerY;
		this.leftText.originX = this.leftText.x;
		////////


		this.rightImage.x = this.game.world.centerX+350;
		this.rightImage.y = this.game.world.centerY;
		this.rightImage.originX = this.rightImage.x;
		this.rightImage.alpha = 0;
		//this.rightImage.rotation = 5;
		
		this.centerImage.x = this.game.world.centerX+155;
		this.centerImage.y = this.game.world.centerY;
		this.centerImage.originX = this.centerImage.x;
		this.centerImage.alpha = 0;

		this.leftImage.x = this.game.world.centerX-200;
		this.leftImage.y = this.game.world.centerY;
		this.leftImage.originX = this.leftImage.x;
		this.leftImage.alpha = 0;

		

		
		//this.leftImage.y-=150;
		this.leftImage.x=-200;
		this.leftImageAn = this.game.add.tween(this.leftImage).to( { alpha:1,x: this.leftImage.originX },1000,Phaser.Easing.Quadratic.Out);
		//this.leftText.y+=150;
		this.leftTextAn = this.game.add.tween(this.leftText).to( { alpha:1,x:this.leftText.originX },1000,Phaser.Easing.Quadratic.Out);

		//this.centerImage.y-=150;
		this.centerImage.x=-200;
		this.centerImageAn = this.game.add.tween(this.centerImage).to( { alpha:1,x:this.centerImage.originX},1000,Phaser.Easing.Quadratic.Out);

		//this.centerText.y+=150;
		this.centerText.x=-200;
		this.centerTextAn = this.game.add.tween(this.centerText).to( { alpha:1,x:this.centerText.originX },1000,Phaser.Easing.Quadratic.Out);
		
		//this.rightImage.y-=150
		this.rightImage.x=-200;
		this.rightImageAn = this.game.add.tween(this.rightImage).to( { alpha:1,x: this.rightImage.originX },1000,Phaser.Easing.Quadratic.Out);

		//this.rightText.y+=150;
		this.rightText.x=-200;
		this.rightTextAn = this.game.add.tween(this.rightText).to( {alpha:1,x:this.rightText.originX},1000,Phaser.Easing.Quadratic.Out);
		

		this.leftImageAn.chain (this.leftTextAn,this.centerImageAn,this.centerTextAn,this.rightImageAn,this.rightTextAn);
		this.leftImageAn.start();
		
		
	},

	update: function()
	{

	},

	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}


